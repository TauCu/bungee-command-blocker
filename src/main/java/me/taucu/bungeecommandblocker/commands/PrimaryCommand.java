package me.taucu.bungeecommandblocker.commands;

import java.util.Arrays;

import me.taucu.bungeecommandblocker.CommandBlockerPlugin;
import me.taucu.bungeecommandblocker.utils.LogUtils;
import me.taucu.bungeecommandblocker.utils.LogUtils.LoggerAttachment;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.md_5.bungee.command.ConsoleCommandSender;

public class PrimaryCommand extends Command implements TabExecutor {
    
    private final CommandBlockerPlugin pl;
    
    private final BaseComponent[] reloading = (new ComponentBuilder()).append("reloading config...")
            .color(ChatColor.GOLD).create();
    private final BaseComponent[] reloaded = (new ComponentBuilder()).append("reloaded!").color(ChatColor.GREEN)
            .create();
    
    public PrimaryCommand(CommandBlockerPlugin pl) {
        super("bungeecommandblocker", "tau.bcmdblock.command", "bcmdblock", "bcmdblocker", "cmdblock", "cmdblocker", "bcb");
        this.pl = pl;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                sender.sendMessage(reloading);
                if (sender instanceof ConsoleCommandSender) {
                    pl.getConfigLoader().reloadConfig();
                } else {
                    try (LoggerAttachment attach = LogUtils.attachLogger(pl.getLogger(), sender)) {
                        pl.getConfigLoader().reloadConfig();
                    }
                }
                sender.sendMessage(reloaded);
            }
        }
    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender s, String[] a) {
        if (a.length > 1) {
            return Arrays.asList("");
        } else {
            return Arrays.asList("reload");
        }
    }
    
}
