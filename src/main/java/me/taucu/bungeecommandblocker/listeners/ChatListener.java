package me.taucu.bungeecommandblocker.listeners;

import me.taucu.bungeecommandblocker.CommandBlockerPlugin;
import me.taucu.bungeecommandblocker.filters.AbstractFilter;
import me.taucu.bungeecommandblocker.filters.Filters;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener implements Listener {
    
    private final Filters filters;
    
    public ChatListener(CommandBlockerPlugin pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onChat(ChatEvent e) {
        if (e.isCommand() && e.getSender() instanceof UserConnection) {
            UserConnection ucon = (UserConnection) e.getSender();
            AbstractFilter filter = filters.apply(ucon, e.getMessage());
            if (filter != null) {
                e.setCancelled(true);
                ucon.sendMessage(filter.getDenyMsg());
            }
        }
    }
    
}
