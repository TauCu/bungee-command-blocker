package me.taucu.bungeecommandblocker.listeners;

import me.taucu.bungeecommandblocker.CommandBlockerPlugin;
import me.taucu.bungeecommandblocker.filters.Filters;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class TabCompleteListener implements Listener {
    
    private final Filters filters;
    
    public TabCompleteListener(CommandBlockerPlugin pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onTab(TabCompleteEvent e) {
        if (e.getSender() instanceof CommandSender) {
            if (filters.apply((CommandSender) e.getSender(), e.getCursor()) != null) {
                e.setCancelled(true);
            }
        }
    }
    
}
