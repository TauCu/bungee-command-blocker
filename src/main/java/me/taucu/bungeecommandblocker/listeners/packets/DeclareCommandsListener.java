package me.taucu.bungeecommandblocker.listeners.packets;

import java.util.Iterator;

import com.mojang.brigadier.tree.CommandNode;
import com.mojang.brigadier.tree.RootCommandNode;

import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.Protocolize;
import dev.simplix.protocolize.api.listener.AbstractPacketListener;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.bungeecommandblocker.CommandBlockerPlugin;
import me.taucu.bungeecommandblocker.filters.Filters;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.protocol.packet.Commands;

public class DeclareCommandsListener extends AbstractPacketListener<Commands> {
    
    private final Filters filters;
    
    public DeclareCommandsListener(CommandBlockerPlugin pl) {
        super(Commands.class, Direction.UPSTREAM, 0);
        this.filters = pl.getFilters();
    }
    
    @Override
    public void packetSend(PacketSendEvent<Commands> e) {
        RootCommandNode<?> root = e.packet().getRoot();
        if (root != null) {
            if (!isBase(root) && filterNode((CommandSender) e.player().handle(), root)) {
                e.cancelled(true);
            } else {
                filterNodes((CommandSender) e.player().handle(), root);
            }
        }
    }
    
    @Override
    public void packetReceive(PacketReceiveEvent<Commands> paramPacketReceiveEvent) {}
    
    private void filterNodes(CommandSender sender, CommandNode<?> node) {
        Iterator<? extends CommandNode<?>> it = node.getChildren().iterator();
        while (it.hasNext()) {
            if (filterNode(sender, it.next())) {
                it.remove();
            }
        }
    }
    
    private boolean filterNode(CommandSender sender, CommandNode<?> node) {
        return filters.apply(sender, "/".concat(node.getName())) != null;
    }
    
    private boolean isBase(CommandNode<?> node) {
        return node.getName().isEmpty();
    }
    
    public void register() {
        Protocolize.listenerProvider().registerListener(this);
    }
    
    public void unregister() {
        try { // for some fucking reason protocolize throws an exception if the listener isn't registered... while providing no method to check if the listener is registered.
            Protocolize.listenerProvider().unregisterListener(this);
        } catch (IllegalArgumentException e) {}
    }
    
}
