package me.taucu.bungeecommandblocker.filters;

public enum FilterAction {
    
    ALLOW, DENY, SOFT_ALLOW, SOFT_DENY, NONE;

}
