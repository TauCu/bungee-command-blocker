package me.taucu.bungeecommandblocker.filters.types;

import me.taucu.bungeecommandblocker.filters.AbstractFilter;
import me.taucu.bungeecommandblocker.filters.FilterAction;
import me.taucu.bungeecommandblocker.filters.Filters;
import net.md_5.bungee.api.CommandSender;

public class Blacklist extends AbstractFilter {
    
    public Blacklist(Filters parent, String name) {
        super(parent, name);
    }
    
    @Override
    public FilterAction apply(CommandSender sender, String command) {
        if (!this.checkPermission(sender) && this.getMatcher(command).find()) {
            return denyAction;
        } else {
            return allowAction;
        }
    }
    
}
