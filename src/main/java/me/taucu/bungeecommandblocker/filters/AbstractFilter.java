package me.taucu.bungeecommandblocker.filters;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import net.md_5.bungee.ServerConnection;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.CommandSender;

public abstract class AbstractFilter {
    
    private final Filters parent;
    
    // cache to prevent excessive queries to the permissions system
    //                  UUID the uuid of the user as the key
    //                  ||||  CacheValue stores the permission result and the current server of the user
    private final Cache<UUID, CacheValue> permissionCache;
    
    String name;
    
    String[] rawPatterns = {};
    Pattern compiled;
    String patternPrefix = "";
    String patternSuffix = "";
    protected FilterAction denyAction = FilterAction.SOFT_DENY;
    protected FilterAction allowAction = FilterAction.SOFT_ALLOW;
    String bypassPerm = "";
    
    String denyMsg = null;
    
    public AbstractFilter(Filters parent, String name) {
        this.parent = parent;
        setName(name);
        resetPatternPrefix();
        resetPatternSuffix();
        resetPermission();
        permissionCache = CacheBuilder.newBuilder()
                .expireAfterWrite(parent.getPermissionCacheMillis(), TimeUnit.MILLISECONDS).build();
    }
    
    public abstract FilterAction apply(CommandSender sender, String command);
    
    public Matcher getMatcher(String command) {
        return compiled.matcher(command);
    }
    
    public boolean checkPermission(CommandSender sender) {
        final String perm = getPermission();
        if (perm != null) {
            if (sender instanceof UserConnection) { // if the sender is a UserConnection, we can use it's UUID in the cache, as it's very fast to hash
                final UserConnection ucon = (UserConnection) sender;
                final CacheValue cached = permissionCache.getIfPresent(ucon.getUniqueId());
                if (cached != null && cached.con == ucon.getServer()) { // return cached value if it exists & is the same server
                    return cached.result;
                } else if (sender.hasPermission(perm)) { // otherwise check perm and cache result
                    permissionCache.put(ucon.getUniqueId(), new CacheValue(true, ucon.getServer()));
                    return true;
                } else {
                    permissionCache.put(ucon.getUniqueId(), new CacheValue(false, ucon.getServer()));
                    return false;
                }
            } else { // otherwise check permission without cache
                return sender.hasPermission(perm);
            }
        }
        return true;
    }
    
    public Filters getParent() {
        return parent;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Pattern getPattern() {
        return compiled;
    }
    
    public String[] getPatterns() {
        return Arrays.copyOf(rawPatterns, rawPatterns.length);
    }
    
    public void setPattern(String... regexes) {
        synchronized (this) {
            this.rawPatterns = Arrays.copyOf(regexes, regexes.length);
            recompile();
        }
    }
    
    public FilterAction getDenyAction() {
        return denyAction;
    }
    
    public FilterAction getAllowAction() {
        return allowAction;
    }
    
    public void setDenyAction(FilterAction denyAction) {
        if (denyAction == null) {
            throw new NullPointerException("denyAction is null");
        }
        this.denyAction = denyAction;
    }
    
    public void setAllowAction(FilterAction allowAction) {
        if (allowAction == null) {
            throw new NullPointerException("allowAction is null");
        }
        this.allowAction = allowAction;
    }
    
    protected void recompile() {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append(patternPrefix);
            sb.append('(');
            sb.append('(');
            if (rawPatterns.length > 0) {
                for (String x : rawPatterns) {
                    sb.append(x + ")|(");
                }
            } else {
                sb.append(')');
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append(')');
            sb.append(patternSuffix);
            this.compiled = Pattern.compile(sb.toString());
        }
    }
    
    public String getPatternPrefix() {
        return patternPrefix;
    }
    
    public void setPatternPrefix(String patternPrefix) {
        synchronized (this) {
            this.patternPrefix = patternPrefix;
            recompile();
        }
    }
    
    public void resetPatternPrefix() {
        setPatternPrefix(parent.getDefaultPatternPrefix());
    }
    
    public String getPatternSuffix() {
        return patternSuffix;
    }
    
    public void setPatternSuffix(String patternSuffix) {
        synchronized (this) {
            this.patternSuffix = patternSuffix;
            recompile();
        }
    }
    
    public void resetPatternSuffix() {
        setPatternSuffix(parent.getDefaultPatternSuffix());
    }
    
    public String getPermission() {
        return bypassPerm;
    }
    
    public void setPermission(String bypass) {
        this.bypassPerm = bypass;
    }
    
    public void resetPermission() {
        setPermission(parent.getRootFilterPerm() + "." + getName());
    }
    
    public String getDenyMsg() {
        return denyMsg == null ? parent.defaultDenyMsg() : denyMsg;
    }
    
    public void setDenyMsg(String denyMsg) {
        this.denyMsg = denyMsg;
    }
    
    public static class CacheValue {
        public final boolean result;
        public final ServerConnection con;
        public CacheValue(boolean result, ServerConnection con) {
            this.result = result;
            this.con = con;
        }
    }
    
}
