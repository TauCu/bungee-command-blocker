package me.taucu.bungeecommandblocker;

import java.io.File;

import me.taucu.bungeecommandblocker.commands.PrimaryCommand;
import me.taucu.bungeecommandblocker.filters.Filters;
import me.taucu.bungeecommandblocker.listeners.ChatListener;
import me.taucu.bungeecommandblocker.listeners.TabCompleteListener;
import me.taucu.bungeecommandblocker.listeners.packets.DeclareCommandsListener;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

public class CommandBlockerPlugin extends Plugin {
    
    private final Filters filters = new Filters();
    
    private final ConfigLoader configLoader = new ConfigLoader(this, new File("./plugins/BungeeCommandBlocker/"));
    
    private DeclareCommandsListener dcl = null;
    
    @Override
    public void onEnable() {
        registerCommands();
        configLoader.reloadConfig();
        registerEvents(new ChatListener(this));
        registerEvents(new TabCompleteListener(this));
        dcl = new DeclareCommandsListener(this);
        dcl.register();
    }
    
    @Override
    public void onDisable() {
        dcl.unregister();
    }
    
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }
    
    public Filters getFilters() {
        return filters;
    }
    
    public void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new PrimaryCommand(this));
    }
    
    public <T extends Listener> T registerEvents(T l) {
        BungeeCord.getInstance().getPluginManager().registerListener(this, l);
        return l;
    }
    
}
