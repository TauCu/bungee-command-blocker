package me.taucu.bungeecommandblocker.utils;

import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import net.md_5.bungee.api.ChatColor;

public class SimpleLogFormatter extends Formatter {
    
    public static final SimpleLogFormatter INSTANCE = new SimpleLogFormatter();
    public static final SimpleLogFormatter CHATCOLOR_INSTANCE = new SimpleChatColorLogFormatter();
    
    public SimpleLogFormatter() {}
    
    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(record.getLevel().getLocalizedName());
        sb.append("]: [");
        sb.append(record.getLoggerName());
        sb.append("] ");
        sb.append(record.getMessage());
        return sb.toString();
    }
    
    public String resolveLevel(LogRecord record) {
        return record.getLevel().getLocalizedName();
    }
    
    private static class SimpleChatColorLogFormatter extends SimpleLogFormatter {
        
        private final ChatColor defaultColor, warnColor, severeColor;
        
        public SimpleChatColorLogFormatter() {
            this(ChatColor.GRAY, ChatColor.YELLOW, ChatColor.RED);
        }
        
        public SimpleChatColorLogFormatter(ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
            this.defaultColor = defaultColor;
            this.warnColor = warnColor;
            this.severeColor = severeColor;
        }
        
        @Override
        public String format(LogRecord record) {
            Level level = record.getLevel();
            ChatColor color;
            switch (level.intValue()) {
            case 900:
                color = warnColor;
                break;
            case 1000:
                color = severeColor;
                break;
            default:
                color = defaultColor;
                break;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(color + "[");
            sb.append(record.getLevel().getLocalizedName());
            sb.append("]: [");
            sb.append(record.getLoggerName());
            sb.append("] ");
            sb.append(record.getMessage());
            return sb.toString();
        }
    }
    
}
